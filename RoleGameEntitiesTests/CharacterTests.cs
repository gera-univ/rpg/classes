using NUnit.Framework;
using RoleGameEntities;

namespace RoleGameClassesTests
{
    [TestFixture]
    public class CharacterTests
    {
        [SetUp]
        public void Setup()
        {
        }

        private Character InitializeCharacterWith3Artifacts()
        {
            var c = DummyFactory.GetCharacter();
            c.PickupArtifact(new ArtifactLivingWater(BottleArtifact.Bottles.small));
            c.PickupArtifact(new ArtifactDeadWater(BottleArtifact.Bottles.small));
            c.PickupArtifact(new ArtifactFrogLegDecoctum());
            return c;
        }

        [Test]
        public void AbandonArtifact_WhenNoArtifactsLeft_ReturnsFalse()
        {
            var c = InitializeCharacterWith3Artifacts();
            Assert.IsTrue(c.AbandonArtifact());
            Assert.IsTrue(c.AbandonArtifact());
            Assert.IsTrue(c.AbandonArtifact());
            Assert.IsFalse(c.AbandonArtifact());
        }

        [Test]
        public void Character_LoopsThroughArtifactsBackward()
        {
            var wizz = InitializeCharacterWith3Artifacts();

            Assert.AreEqual(0, wizz.NActiveArtifact);
            wizz.PreviousArtifact();
            Assert.AreEqual(2, wizz.NActiveArtifact);
            wizz.PreviousArtifact();
            Assert.AreEqual(1, wizz.NActiveArtifact);
            wizz.PreviousArtifact();
            Assert.AreEqual(0, wizz.NActiveArtifact);
        }

        [Test]
        public void Character_LoopsThroughArtifactsForward()
        {
            var wizz = InitializeCharacterWith3Artifacts();

            Assert.AreEqual(0, wizz.NActiveArtifact);
            wizz.NextArtifact();
            Assert.AreEqual(1, wizz.NActiveArtifact);
            wizz.NextArtifact();
            Assert.AreEqual(2, wizz.NActiveArtifact);
            wizz.NextArtifact();
            Assert.AreEqual(0, wizz.NActiveArtifact);
        }

        [Test]
        public void Character_PassesArtifactToOtherCharacter()
        {
            var vasya = DummyFactory.GetCharacter(health: 5, maxHealth: 10);
            var petya = DummyFactory.GetCharacter(health: 5, maxHealth: 10);
            vasya.PickupArtifact(new ArtifactLivingWater(BottleArtifact.Bottles.small));

            vasya.PassArtifact(petya);

            vasya.UseArtifact(vasya);
            Assert.AreEqual(5, vasya.Health); // у Васи нету артифакта

            petya.UseArtifact(petya);
            Assert.AreEqual(10, petya.Health); // у Пети есть артифакт
        }

        [Test]
        public void Character_UsesArtifactOnSelf()
        {
            var c = DummyFactory.GetCharacter(health: 5, maxHealth: 10);
            c.PickupArtifact(new ArtifactLivingWater(BottleArtifact.Bottles.small));
            c.UseArtifact(c);
            Assert.AreEqual(10, c.Health);
        }

        [Test]
        public void Character_UsesArtifactOnTarget()
        {
            var c = DummyFactory.GetCharacter();
            var target = DummyFactory.GetCharacter(health: 5, maxHealth: 10);
            c.PickupArtifact(new ArtifactLivingWater(BottleArtifact.Bottles.small));
            c.UseArtifact(target);
            Assert.AreEqual(10, target.Health);
        }

        [Test]
        public void Character_WhenDead_CantBecomeNormal()
            /*
             * https://www.youtube.com/watch?v=vnciwwsvNcc
             */
        {
            var Parrot = new Character("Norwegian Blue", CharacterRace.Parrot, CharacterGender.Male)
                {Health = 0, MaxHealth = 15};
            Assert.AreEqual(CharacterCondition.Dead, Parrot.Condition);
            Parrot.Health += 15;
            Assert.AreEqual(CharacterCondition.Dead, Parrot.Condition);
        }

        [Test]
        public void Character_WhenHealthExceedsMaxHealth_CapsToMaxHealth()
        {
            var c = DummyFactory.GetCharacter(health: 50, maxHealth: 50);
            c.Health += 5;
            Assert.AreEqual(c.MaxHealth, c.Health);
        }

        [Test]
        public void Character_WhenHealthSubceedsZero_SetsToZero()
        {
            var c = DummyFactory.GetCharacter(health: 50, maxHealth: 50);
            c.Health -= 100;
            Assert.AreEqual(0, c.Health);
        }

        [Test]
        public void Character_WhenWeakened_CanBecomeNormal()
        {
            var Anakin = new Character("Anakin", CharacterRace.Human, CharacterGender.Male)
                {Health = 1, MaxHealth = 1000};
            Assert.AreEqual(CharacterCondition.Weakened, Anakin.Condition);
            Anakin.Health += 500;
            Assert.AreEqual(CharacterCondition.Normal, Anakin.Condition);
        }

        [Test]
        public void Characters_ComparedByExperience()
        {
            var Vasya = new Character("Вася", CharacterRace.Human, CharacterGender.Male) {Experience = 500};
            var Petya = new Character("Петя", CharacterRace.Human, CharacterGender.Male) {Experience = 1000};

            Assert.AreEqual(-1, Vasya.CompareTo(Petya));
        }

        [Test]
        public void Characters_HaveIDs()
        {
            var Adam = new Character("Adam", CharacterRace.Human, CharacterGender.Male);
            var Eve = new Character("Eve", CharacterRace.Human, CharacterGender.Female);
            Assert.AreEqual(Adam.Id + 1, Eve.Id);
        }

        [Test]
        public void UseArtifact_WhenCharacterIsDead_ReturnsFalse()
        {
            var c = DummyFactory.GetCharacter();
            c.PickupArtifact(new ArtifactDeadWater(BottleArtifact.Bottles.small));
            c.Condition = CharacterCondition.Dead;
            Assert.IsFalse(c.UseArtifact(c));
        }

        [Test]
        public void UseArtifact_WhenInventoryIsEmpty_ReturnsFalse()
        {
            var c = DummyFactory.GetCharacter();
            Assert.IsFalse(c.UseArtifact(c));
        }

    }
}