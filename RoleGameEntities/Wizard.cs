﻿using System.Collections.Generic;

namespace RoleGameEntities
{
    public class Wizard : Character
    {
        private bool _isMaxMagickaSet = false;
        private int _magicka;
        private int _maxMagicka;

        private List<Spell> _spells = new List<Spell>();
        public int NActiveSpell { get; private set; }

        public int Magicka
        {
            get => _magicka;
            set
            {
                _magicka = value;
                if (!_isMaxMagickaSet)
                    MaxMagicka = _magicka;
                UpdateCondition();
            }
        }


        public int MaxMagicka
        {
            get => _maxMagicka;
            set
            {
                _maxMagicka = value;
                _isMaxMagickaSet = true;
            }
        }

        public Wizard(string name, CharacterRace race, CharacterGender gender) : base(name, race, gender)
        {
        }

        public override void UpdateCondition()
        {
            base.UpdateCondition();

            if (_magicka > _maxMagicka)
                _magicka = _maxMagicka;
            if (_magicka <= 0)
                _magicka = 0;

            if (_spells.Count == 0)
                NActiveSpell = 0;
            else if (NActiveSpell >= _spells.Count)
                NActiveSpell = 0;
            else if (NActiveSpell < 0)
                NActiveSpell = _spells.Count - 1;
        }

        public bool Cast(Character target)
        {
            var spell = _spells[NActiveSpell];
            if (Magicka < spell.Cost ||
                spell.IsMotoric && !CanMove || spell.IsVerbal && !CanTalk || Condition == CharacterCondition.Dead )
                return false;
            Magicka -= spell.Cost;
            spell.Affect(target);
            return true;
        }

        public void LearnSpell(Spell s)
        {
            _spells.Add(s);
        }

        public bool ForgetSpell()
        {
            if (_spells.Count == 0)
                return false;
            _spells.RemoveAt(NActiveSpell);
            UpdateCondition();
            return true;
        }

        public void NextSpell()
        {
            ++NActiveSpell;
            UpdateCondition();
        }

        public void PreviousSpell()
        {
            --NActiveSpell;
            UpdateCondition();
        }
    }
}