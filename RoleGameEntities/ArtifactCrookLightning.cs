﻿namespace RoleGameEntities
{
    public class ArtifactCrookLightning : PoweredArtifact
    {
        public ArtifactCrookLightning (int power, bool isDisposable = false) : base(power, isDisposable)
        {
        
        }

        public override void Affect(Character target)
        {
            target.Health -= Power;
        }
    }
}
