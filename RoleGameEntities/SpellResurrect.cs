﻿namespace RoleGameEntities
{
    public class SpellResurrect : Spell
    {
        public SpellResurrect(int cost = 150, bool isVerbal = false, bool isMotoric = true)
        {
            Cost = cost;
            IsVerbal = isVerbal;
            IsMotoric = isMotoric;
        }

        public override void Affect(Character target)
        {
            if (target.Condition == CharacterCondition.Dead)
            {
                target.Condition = CharacterCondition.Normal;
                target.Health = 1;
            }

            target.UpdateCondition();
        }
    }
}