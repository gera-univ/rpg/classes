﻿namespace RoleGameEntities
{
    public abstract class PoweredSpell : Spell
    {
        private int _cost;
        public int Power { get; set; }
        public sealed override int Cost
        {
            get => Power*_cost;
            set => _cost = value;
        }
    }
}