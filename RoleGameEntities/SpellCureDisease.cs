﻿namespace RoleGameEntities
{
    public class SpellCureDisease : Spell
    {
        public SpellCureDisease(int cost = 20, bool isVerbal = false, bool isMotoric = true)
        {
            Cost = cost;
            IsVerbal = isVerbal;
            IsMotoric = isMotoric;
        }

        public override void Affect(Character target)
        {
            if (target.Condition == CharacterCondition.Sick)
                target.Condition = CharacterCondition.Normal;
            target.UpdateCondition();
        }
    }
}