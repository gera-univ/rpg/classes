﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoleGameEntities
{
    public class ArtifactBasiliskEye : Artifact
    {
        public ArtifactBasiliskEye(bool isDisposable = true) : base(isDisposable) 
        {
        }

        public override void Affect(Character target)
        {
            if (target.Condition != CharacterCondition.Dead)
                target.Condition = CharacterCondition.Paralyzed;
            target.UpdateCondition();
        }
    }
}
