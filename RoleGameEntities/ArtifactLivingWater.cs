﻿namespace RoleGameEntities
{
    public class ArtifactLivingWater : BottleArtifact
    {
        public ArtifactLivingWater(Bottles size, bool isDisposable = true) : base(size, isDisposable)
        {
        }

        public override void Affect(Character target)
        {
            target.Health += Power;
        }
    }
}