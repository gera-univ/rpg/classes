﻿namespace RoleGameEntities
{
    public class SpellRelease : Spell
    {
        public SpellRelease(int cost = 85, bool isVerbal = true, bool isMotoric = false)
        {
            Cost = cost;
            IsVerbal = isVerbal;
            IsMotoric = isMotoric;
        }

        public override void Affect(Character target)
        {
            if (target.Condition == CharacterCondition.Paralyzed)
            {
                target.Condition = CharacterCondition.Normal;
                target.Health = 1;
            }

            target.UpdateCondition();
        }
    }
}