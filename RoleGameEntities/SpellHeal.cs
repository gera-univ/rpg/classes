﻿namespace RoleGameEntities
{
    public class SpellHeal : PoweredSpell
    {
        public SpellHeal(int power, int cost = 2, bool isVerbal = false, bool isMotoric = true)
        {
            Cost = cost;
            Power = power;
            IsVerbal = isVerbal;
            IsMotoric = isMotoric;
        }

        public override void Affect(Character target)
        {
            target.Health += Power;
        }
    }
}