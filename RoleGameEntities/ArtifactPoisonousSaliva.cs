﻿namespace RoleGameEntities
{
    public class ArtifactPoisonousSaliva : PoweredArtifact
    {
        public ArtifactPoisonousSaliva(int power, bool isDisposable = false) : base(power, isDisposable)
        { }

        public override void Affect(Character target)
        {
            if (target.Condition == CharacterCondition.Normal || target.Condition == CharacterCondition.Weakened)
            {
                target.Condition = CharacterCondition.Poisoned;
                target.Health -= Power;
            }
        }
    }
}
