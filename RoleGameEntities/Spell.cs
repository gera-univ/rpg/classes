﻿namespace RoleGameEntities
{
    public abstract class Spell : IMagick
    {
        public virtual int Cost { get; set; } = 0;
        public abstract void Affect(Character target);
        public virtual bool IsVerbal { get; set; }
        public virtual bool IsMotoric { get; set; }
    }
}