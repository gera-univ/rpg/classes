﻿using System.Timers;

namespace RoleGameEntities
{
    public abstract class TemporarySpell : PoweredSpell
    {
        private Timer _expTimer;

        private Character _target;

        public TemporarySpell(int time)
        {
            Power = time;
        }

        public override void Affect(Character target)
        {
            _target = target;
            _expTimer = new Timer(Power);
            _expTimer.Elapsed += OnTimerElapsed;
            _expTimer.AutoReset = false;
            _expTimer.Enabled = true;
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            Disaffect(_target);
        }

        public abstract void Disaffect(Character target);
    }
}