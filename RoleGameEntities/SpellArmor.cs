﻿namespace RoleGameEntities
{
    public class SpellArmor : TemporarySpell
    {
        public SpellArmor(int time) : base(time)
        {
        }

        public override void Affect(Character target)
        {
            base.Affect(target);
            target.IsInvincible = true;
        }

        public override void Disaffect(Character target)
        {
            target.IsInvincible = false;
        }
    }
}