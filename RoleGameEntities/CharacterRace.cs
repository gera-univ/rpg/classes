﻿namespace RoleGameEntities
{
    public enum CharacterRace
    {
        Human,
        Dwarf,
        Elf,
        Ork,
        Goblin,
        Parrot,
        Dummy
    }
}